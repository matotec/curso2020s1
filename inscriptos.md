# Alumnos inscriptos al Parcial 2020 s1

 Para rendir el parcial los alumnos deberán tener entregada la practica 4 antes del dia 29/06

| Legajo    | Apellido | Nombre     |  Grupo | Rinde Parcial |
|  -----    | -----   | -----      |  ----- | ----- | 
|	38765	|	Arciniega Marchini	|	Camila		| Grupo 7	| Si |
|	36726	|	Benitez	|	Tamara Elizabeth		|Grupo 14	| Si |
|	39383	|	Bianchi	|	Matias Ezequiel		|Grupo 12 	| Si |
|	35069	|	Calvento	|	Tobias		| Grupo 4| Si |
|	39130	|	Cameriere	|	Federico		| Grupo 6	| Si |
|	38250	|	Cardozo	|	Matias Leandro		| Grupo 7	| Si |
|	38495	|	Chaile	|	Matias Ezequiel		|Grupo 13 	| Si |
|	34989	|	Fernandez	|	Flavia Ivana		|Grupo 13	| Si |
|	29328	|	Garcia	|	Rodrigo Nahuel	 	| Grupo 3	| Si |
|	32441	|	Gareca	|	Jorge Alejandro		|Grupo 9	| Si |
|	39816	|	Guasch	|	Gonzalo		|Grupo 8 	| Si |
|	37554	|	Lopez	|	Eva Nadia		|Grupo 15	| Si |
|	35488	|	Lopez Del Castillo	|	Mariano Nicolas	| Grupo 2 | Si |
|	32384	|	Maroso	|	Lucas Matias		| Grupo 3	| Si |
|	28450	|	Martinez	|	Alan Leonardo		|Grupo 9 	| Si |
|	35706	|	Moronha	|	Diego Hernan		|Grupo 14	| Si |
|	42487	|	Padron	|	Angelo Daniel		|Grupo 16	| Si |
|	35173	|	Rodriguez Cisneros	|	Leonardo Maximiliano		| 	Grupo 2	| Si |
|	32588	|	Rodriguez	|	Nicolas Leonel	| Grupo 4	| Si |
|	26631	|	Rovere	|	Melanie Soledad		|Grupo 17	| Si |
|	31716	|	Salas Castromonte	|	Luis Alfredo	|Grupo 15	| Si |
|	38721	|	Tarnovski	|	Jose Maximiliano	| Grupo 6	| Si |
|	36046	|	Torres	|	Tobias	|	 Grupo 7	| Si |
|	37854	|	Veron	|	Gonzalo Gabriel	|Grupo 5	| Si |
|	33791	|	Wojciechow	|	Lucas	|Grupo 8 	| Si |

